import datetime
from flask.ext.restful import fields
from flask import url_for
from urlparse import urlparse, urlunparse


class Date(fields.Raw):
    def format(self, value):
        if isinstance(value, datetime.date):
            return value.strftime('%Y-%m-%d')
        return value


class Time(fields.Raw):
    def format(self, value):
        if isinstance(value, datetime.time):
            return value.strftime('%H:%M:%S')
        return value


class DateTime(fields.Raw):
    def format(self, value):
        if isinstance(value, datetime.datetime):
            return value.strftime('%Y-%m-%d %H:%M:%S')
        return value
        
        
class Url(fields.Url):
    def output(self, key, obj):
        try:
            data = fields.to_marshallable_type(obj)
            if all(x in data.keys() for x in ['table_name', 'schema']):
                if data['schema'] != 'public':
                    data['table_name'] = '%s.%s' % (data['schema'], data['table_name'])
            o = urlparse(url_for(self.endpoint, _external=self.absolute, **data))
            if self.absolute:
                scheme = self.scheme if self.scheme is not None else o.scheme
                return urlunparse((scheme, o.netloc, o.path, "", "", ""))
            return urlunparse(("", "", o.path, "", "", ""))
        except TypeError as te:
            raise fields.MarshallingException(te)