from collections import OrderedDict

from flask.ext.restful import Resource, reqparse, fields, marshal
from flask import jsonify, make_response, request

from src import db
from src import customfields
from src.auth import auth


class MissingTableException(Exception):
    pass


def get_columns(tables_name, schema_name=None):
    get_columns_sql = "SELECT column_name, udt_name FROM information_schema.columns WHERE table_name = '{tables_name}'"

    type_map = {
        'int2': 'int',
        'int4': 'int',
        'int8': 'int',
        'numeric': 'float',
        'float4': 'float',
        'float8': 'float',
        'varchar': 'str',
        'text': 'str',
        'date': 'date',
        'time': 'time',
        'timestamp': 'datetime',
        'bool': 'bool',
    }

    if schema_name:
        get_columns_sql = get_columns_sql + " AND table_schema = '%s'" % schema_name + ';'
    else:
        get_columns_sql = get_columns_sql + ';'

    with db.connect(commit=False) as cur:
        cur.execute(get_columns_sql.format(tables_name=tables_name))
        rows = cur.fetchall()

    if not rows:
        raise MissingTableException

    return [{row[0]: type_map[row[1]]} for row in rows]


def build_where_clause(params, table_fields):
    where_clause = []
    for k, v in params.iteritems():
        if isinstance(table_fields[k], fields.Integer):
            where_clause.append("%s = %s" % (k, v))
        else:
            where_clause.append("%s = '%s'" % (k, v))
    return ' AND '.join(where_clause)


def get_fields_dict(columns):
    type_fields = {
        'int': fields.Integer,
        'float': fields.Float,
        'str': fields.String,
        'date': customfields.Date,
        'time': customfields.Time,
        'datetime': customfields.DateTime,
        'bool': fields.Boolean,
    }

    table_fields = OrderedDict()
    for column in columns:
        for column_name, column_type in column.iteritems():
            table_fields[column_name] = type_fields[column_type]
    return table_fields


def split_table_name(table_name):
    table_parts = table_name.split('.')
    if len(table_parts) == 2:
        return table_parts[0], table_parts[1]
    return 'public', table_parts[0]


class SchemaList(Resource):
    ROUTE = '/schemas'
    ENDPOINT = 'schemaslist'

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            'schema_name',
            required=True,
            type=str,
            help='No schema name provided',
            location='json'
        )
        super(SchemaList, self).__init__()

    @auth.login_required
    def get(self):
        get_schema_sql = 'SELECT schema_name FROM information_schema.schemata;'

        with db.connect(commit=False) as cur:
            cur.execute(get_schema_sql)
            rows = cur.fetchall()

        schemas = [row[0] for row in rows]
        return make_response(jsonify({'schemas': schemas}))

    @auth.login_required
    def post(self):
        create_schema_sql = 'CREATE SCHEMA {schema_name}'

        args = self.reqparse.parse_args()
        schema_name = args['schema_name']
        with db.connect(commit=True) as cur:
            cur.execute(create_schema_sql.format(schema_name=schema_name))

        return make_response(jsonify({'schema_name': schema_name, 'action': 'create'}))


class Schema(Resource):
    ROUTE = '/schemas/<string:schema_name>'
    ENDPOINT = 'schemas'

    @auth.login_required
    def delete(self, schema_name):

        drop_schema_sql = 'DROP SCHEMA {schema_name} CASCADE;'

        with db.connect(commit=True) as cur:
            cur.execute(drop_schema_sql.format(schema_name=schema_name))

        return make_response(jsonify({'schema_name': schema_name, 'action': 'delete'}))


class TableList(Resource):
    ROUTE = '/tables'
    ENDPOINT = 'tableslist'

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            'table_name',
            required=True,
            type=str,
            help='No table name provided',
            location='json'
        )
        self.reqparse.add_argument(
            'columns',
            required=True,
            type=list,
            help='No table columns provided',
            location='json'
        )
        super(TableList, self).__init__()

    @auth.login_required
    def get(self):
        get_table_sql = "SELECT schemaname, relname, n_live_tup FROM pg_stat_user_tables;"

        table_fields = {
            'table_name': fields.String,
            'schema': fields.String,
            'columns': fields.Raw,
            'rows': fields.Integer,
            'uri': customfields.Url('tables', absolute=True, scheme='https')
        }

        with db.connect(commit=False) as cur:
            cur.execute(get_table_sql)
            rows = cur.fetchall()

        tables = []
        for row in rows:
            columns = get_columns(row[1])
            data = {'table_name': row[1], 'schema': row[0], 'rows': row[2], 'columns': columns}
            marshaled = marshal(data, table_fields)
            tables.append(marshaled)

        return make_response(jsonify({'tables': tables}))

    @auth.login_required
    def post(self):
        tmap = {
            'int': 'integer',
            'float': 'decimal',
            'str': 'text',
            'date': 'date',
            'time': 'time',
            'datetime': 'timestamp',
            'bool': 'boolean'
        }
        create_table_sql = 'CREATE TABLE {schema_name}.{table_name} ({columns});'

        args = self.reqparse.parse_args()
        schema_name, table_name = split_table_name(args['table_name'])
        columns = []
        for column in args['columns']:
            for column_name, column_type in column.iteritems():
                try:
                    columns.append(
                        '{column_name} {column_type}'.format(
                            column_name=column_name,
                            column_type=tmap[column_type]
                        )
                    )
                except KeyError:
                    return make_response(jsonify({'message': 'Invalid schema'}), 400)

        with db.connect(commit=True) as cur:
            cur.execute(
                create_table_sql.format(
                    schema_name=schema_name,
                    table_name=table_name,
                    columns=', '.join(columns)
                )
            )

        return make_response(jsonify({'table_name': table_name, 'action': 'create'}))


class Table(Resource):
    ROUTE = '/tables/<string:table_name>'
    ENDPOINT = 'tables'

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('columns', type=list, help='No data provided', location='json')
        self.reqparse.add_argument('data', type=list, required=True, help='No data provided', location='json')
        super(Table, self).__init__()

    @auth.login_required
    def get(self, table_name):
        schema_name, table_name = split_table_name(table_name)
        try:
            columns = get_columns(table_name, schema_name)
        except MissingTableException:
            return make_response(jsonify({"message": "Unable to find table '%s'" % table_name}), 404)

        table_fields = get_fields_dict(columns)

        # Check to see if we are filtering on a specific column
        params = request.args
        if params:
            select_table_sql = 'SELECT * FROM {schema_name}.{table_name} WHERE ' \
                               + build_where_clause(params, table_fields) + ';'
        else:
            select_table_sql = 'SELECT * FROM {schema_name}.{table_name};'

        with db.connect(commit=False) as cur:
            cur.execute(select_table_sql.format(schema_name=schema_name, table_name=table_name))
            rows = cur.fetchall()

        data = []
        for row in rows:
            d = dict(zip([column_name for column in columns for column_name in column.iterkeys()], row))
            marshaled = marshal(d, table_fields)
            data.append([v for v in marshaled.itervalues()])

        return make_response(jsonify({'table_name': table_name, 'columns': columns, 'data': data}))

    @auth.login_required
    def post(self, table_name):
        schema_name, table_name = split_table_name(table_name)
        args = self.reqparse.parse_args()
        if args['columns']:
            sql = 'INSERT INTO {schema_name}.{table_name} ({column_names}) VALUES {values};'
            insert_into_sql = sql.format(column_names=','.join(args['column_names']))
        else:
            insert_into_sql = 'INSERT INTO {schema_name}.{table_name} VALUES {values};'

        with db.connect(commit=True) as cur:
            values = ','.join(cur.mogrify('(' + ','.join(['%s']*len(data)) + ')', data) for data in args['data'])
            cur.execute(insert_into_sql.format(schema_name=schema_name, table_name=table_name, values=values))

        return make_response(jsonify({'table_name': table_name, 'action': 'insert'}))

    @auth.login_required
    def delete(self, table_name):
        schema_name, table_name = split_table_name(table_name)
        try:
            columns = get_columns(table_name, schema_name)
        except MissingTableException:
            return make_response(jsonify({"message": "Unable to find table '%s'" % table_name}), 404)

        table_fields = get_fields_dict(columns)

        params = request.args
        if params:
            sql = 'DELETE FROM {schema_name}.{table_name} WHERE ' + build_where_clause(params, table_fields) + ';'
        else:
            sql = 'DROP TABLE {schema_name}.{table_name};'

        with db.connect(commit=True) as cur:
            cur.execute(sql.format(schema_name=schema_name, table_name=table_name))

        return make_response(jsonify({'table_name': table_name, 'action': 'delete'}))