import os
import urlparse
import psycopg2
from contextlib import contextmanager

urlparse.uses_netloc.append('postgres')
url = urlparse.urlparse(os.environ["DATABASE_URL"])

@contextmanager
def connect(commit):
    try:
        conn = psycopg2.connect(
            database=url.path[1:],
            user=url.username,
            password=url.password,
            host=url.hostname,
            port=url.port
        )
        cur = conn.cursor()
        yield cur
    finally:
        if commit:
            conn.commit()
        cur.close()
        conn.close()