import os

from flask import make_response, jsonify
from flask.ext.httpauth import HTTPBasicAuth

auth = HTTPBasicAuth()


@auth.get_password
def get_password(username):
    if username == os.environ["USERNAME"]:
        return os.environ["PASSWORD"]
    return None


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'message': 'Unauthorized access'}), 401)