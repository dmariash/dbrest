from flask import Flask
from flask.ext.restful import Api
from flask.views import MethodViewType

from src import resources


app = Flask(__name__)
api = Api(app)


for c in dir(resources):
    clazz = getattr(resources, c)
    if isinstance(clazz, MethodViewType):
        if all(x in dir(clazz) for x in ['ROUTE', 'ENDPOINT']):
            api.add_resource(clazz, clazz.ROUTE, endpoint=clazz.ENDPOINT)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)