import requests
import json

auth = ('an_authentication', 'tuple')
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}


table_example = {'table_name': 'example', 'columns': [
    {'id': 'int'},
    {'name': 'str'},
    {'amount': 'float'},
    {'exists': 'bool'},
    {'date': 'date'},
    {'time': 'time'}
]}


table_example_data = {'data': [
    [1, 'dan', 1.5, True, '2014-05-30', '10:14:00'],
    [2, 'dan', 3.1, False, '2014-05-30', '10:15:00'],
    [3, 'jen', 8.26, True, '2014-05-29', '18:32:11'],
    [4, 'jen', 6.34, True, '2014-05-30', '15:12:10'],
    [5, 'chris', 4.33, False, '2014-05-16', '05:19:14'],
    [6, 'harry', 56.32, False, '2014-04-19', '12:00:00'],
    [7, 'harry', 7.26, True, '2014-05-24', '13:08:11'],
    [8, 'harry', 93.22, True, '2014-05-12', '13:19:06'],
]}


schema_example = {'schema_name': 'dan'}


table_example_2 = {'table_name': 'dan.schemaexample', 'columns': [
    {'id': 'int'},
    {'name': 'str'}
]}


table_example_data_2 = {'data': [
    [1, 'dan'],
    [2, 'dan'],
    [3, 'harry'],
    [4, 'chris']
]}


def insert():
    requests.post('http://192.168.1.8:5000/tables', data=json.dumps(table_example), auth=auth, headers=headers)
    requests.post('http://192.168.1.8:5000/tables/example', data=json.dumps(table_example_data), auth=auth, headers=headers)
    requests.post('http://192.168.1.8:5000/schemas', data=json.dumps(schema_example), auth=auth, headers=headers)
    requests.post('http://192.168.1.8:5000/tables', data=json.dumps(table_example_2), auth=auth, headers=headers)
    requests.post('http://192.168.1.8:5000/tables/dan.schemaexample', data=json.dumps(table_example_data_2), auth=auth, headers=headers)

def clear():
    data = requests.get('https://dbrest.herokuapp.com/tables', auth=auth).json()
    table_names = ['%s.%s' % (x[u'schema'], x[u'table_name']) for x in data[u'tables']]
    for table in table_names:
        requests.delete('http://192.168.1.8:5000/tables/%s' % table, auth=auth)
    requests.delete('https://dbrest.herokuapp.com/schemas/dan', auth=auth)

if __name__ == '__main__':
    insert()
